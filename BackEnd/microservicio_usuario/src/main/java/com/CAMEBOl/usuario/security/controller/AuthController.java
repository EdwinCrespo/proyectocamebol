package com.CAMEBOl.usuario.security.controller;

import org.aspectj.weaver.NewConstructorTypeMunger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.CAMEBOl.usuario.security.dto.JwtDto;
import com.CAMEBOl.usuario.security.dto.LoginUsuario;
import com.CAMEBOl.usuario.security.dto.NuevoUsuario;
import com.CAMEBOl.usuario.security.entity.Mensaje;
import com.CAMEBOl.usuario.security.entity.Rol;
import com.CAMEBOl.usuario.security.entity.Usuario;
import com.CAMEBOl.usuario.security.enums.RolNombre;
import com.CAMEBOl.usuario.security.jwt.JwtProvider;
import com.CAMEBOl.usuario.security.service.RolService;
import com.CAMEBOl.usuario.security.service.UsuarioService;

import javax.validation.Valid;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
@RequestMapping("/auth")
@CrossOrigin(origins = "*")
public class AuthController {
	 @Autowired
	    PasswordEncoder passwordEncoder;

	    @Autowired
	    AuthenticationManager authenticationManager;

	    @Autowired
	    UsuarioService usuarioService;

	    @Autowired
	    RolService rolService;

	    @Autowired
	    JwtProvider jwtProvider;

	    @PostMapping("/nuevo")
	    public ResponseEntity<?> nuevo(@Valid @RequestBody NuevoUsuario nuevoUsuario, BindingResult bindingResult){
	        if(bindingResult.hasErrors())
	            return new ResponseEntity(new Mensaje("campos mal puestos o email inválido"), HttpStatus.BAD_REQUEST);
	        if(usuarioService.existsByNombreUsuario(nuevoUsuario.getNombreUsuario()))
	            return new ResponseEntity(new Mensaje("ese nombre ya existe"), HttpStatus.BAD_REQUEST);
	        if(usuarioService.existsByEmail(nuevoUsuario.getEmail()))
	            return new ResponseEntity(new Mensaje("ese email ya existe"), HttpStatus.BAD_REQUEST);
	        Usuario usuario =
	                new Usuario(nuevoUsuario.getNombre(), nuevoUsuario.getNombreUsuario(), nuevoUsuario.getEmail(),
	                        passwordEncoder.encode(nuevoUsuario.getPassword()),nuevoUsuario.getPrimerApellido(),nuevoUsuario.getFechaNacimiento(),nuevoUsuario.getIdEmpresa(),"1");
	        Set<Rol> roles = new HashSet<>();
	        roles.add(rolService.getByRolNombre(RolNombre.ROLE_USER).get());
	        if(nuevoUsuario.getRoles().contains("admin"))
	            roles.add(rolService.getByRolNombre(RolNombre.ROLE_ADMIN).get());
	        usuario.setRoles(roles);
	        //Validaciones de campos no obligatorios
	        if(nuevoUsuario.getSegundoApellido()!="" || nuevoUsuario.getSegundoApellido()!=null) {
		        usuario.setSegundoApellido(nuevoUsuario.getSegundoApellido().toString());
	        }
	        if(nuevoUsuario.getCi()!="" || nuevoUsuario.getCi()!=null) {
		        usuario.setCi(nuevoUsuario.getCi().toString());
	        }
	        if(nuevoUsuario.getGenero()!="" || nuevoUsuario.getGenero()!=null) {
		        usuario.setGenero(nuevoUsuario.getGenero().toString());
	        }
	        if(nuevoUsuario.getNumeroTelefono()!="" || nuevoUsuario.getNumeroTelefono()!=null) {
		        usuario.setNumeroTelefono(nuevoUsuario.getNumeroTelefono().toString());
	        }

	        usuarioService.save(usuario);
	        return new ResponseEntity(new Mensaje("Usuario creado"),HttpStatus.CREATED);
	    }

	    @PostMapping("/login")
	    public ResponseEntity<JwtDto> login(@Valid @RequestBody LoginUsuario loginUsuario, BindingResult bindingResult){
	        if(bindingResult.hasErrors())
	            return new ResponseEntity("campos mal puestos", HttpStatus.BAD_REQUEST);
	        Authentication authentication =
	                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginUsuario.getNombreUsuario(), loginUsuario.getPassword()));
	        SecurityContextHolder.getContext().setAuthentication(authentication);
	        String jwt = jwtProvider.generateToken(authentication);
	        UserDetails userDetails = (UserDetails)authentication.getPrincipal();
	        Usuario usuarioIdAyuda=usuarioService.getByNombreUsuarioOrEmail(loginUsuario.getNombreUsuario()).get();
	        
	        JwtDto jwtDto = new JwtDto(jwt, userDetails.getUsername(), usuarioIdAyuda.getId(), userDetails.getAuthorities());
	        return new ResponseEntity(jwtDto, HttpStatus.OK);
	    }

	    
}
