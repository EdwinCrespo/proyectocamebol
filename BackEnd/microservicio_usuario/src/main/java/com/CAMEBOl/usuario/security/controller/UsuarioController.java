package com.CAMEBOl.usuario.security.controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.http.HttpHeaders;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.CAMEBOl.usuario.security.entity.Mensaje;
import com.CAMEBOl.usuario.security.entity.Usuario;
import com.CAMEBOl.usuario.security.service.EmailService;
import com.CAMEBOl.usuario.security.service.UsuarioService;
import com.ctc.wstx.shaded.msv_core.verifier.ErrorInfo;
import com.google.common.io.Files;

import antlr.StringUtils;

@RestController
@RequestMapping("/auth/user")
@CrossOrigin(origins = "*")
public class UsuarioController {

	
	String mensaje="";
	Integer estado=0;
	String respuesta="";
	String fallo="";
	@Autowired
	UsuarioService usuarioService;
	
	@GetMapping("/lista")
	public ResponseEntity<List<Usuario>> getAll(){
		List<Usuario> list = usuarioService.getAll();
		return  new ResponseEntity<List<Usuario>>(list,HttpStatus.OK);
	}
	@GetMapping("/listaId/{id}")
	public ResponseEntity<Usuario> getById(@PathVariable("id") int id){
		if(!usuarioService.existsById(id)) {
			return new ResponseEntity(new Mensaje("El id no existe"),HttpStatus.NOT_FOUND);
		}
		Usuario usuario=usuarioService.getById(id).get();
		return new ResponseEntity(usuario,HttpStatus.OK);
	}
	@GetMapping("/listaEstado/{estado}")
	public ResponseEntity<List<Usuario>> getByEstado(@PathVariable("estado")String estado){
		List<Usuario> list = usuarioService.getByEstado(estado);
		return  new ResponseEntity<List<Usuario>>(list,HttpStatus.OK);
	}
	@DeleteMapping("/elimnar/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id){
		usuarioService.delete(id);
		return new ResponseEntity(new Mensaje("Usuario Eliminado"),HttpStatus.OK);
	}
	
	@PutMapping("/actualizar/{id}")
	public ResponseEntity<?> actualizar(@PathVariable("id") int id, @RequestBody Usuario usuario){
//		if(!usuarioService.existsById(id)) {
//			return new ResponseEntity(new Mensaje("El id no existe"),HttpStatus.NOT_FOUND);
//		}
			
//		if(!usuarioService.existsByNombreUsuario(usuario.getNombreUsuario())) 
//		{
//			return new ResponseEntity(new Mensaje("El nombre de usuario ya existe"),HttpStatus.NOT_FOUND);
//		}
			
		Usuario usuario2=usuarioService.getById(id).get();
		usuario2.setNombre(usuario.getNombre());
		usuario2.setEmail(usuario.getEmail());
		usuario2.setPrimerApellido(usuario.getPrimerApellido());
		usuario2.setSegundoApellido(usuario.getSegundoApellido());
		usuario2.setFechaNacimiento(usuario.getFechaNacimiento());
		usuario2.setNumeroTelefono(usuario.getNumeroTelefono());
		usuario2.setCi(usuario.getCi());
		usuario2.setEstado(usuario.getEstado());
				
		usuarioService.save(usuario2);
		return new ResponseEntity(new Mensaje("Usuario Actualizado"),HttpStatus.OK);
	
	}	
    
    @PostMapping("/subirFoto")
    public ResponseEntity<?> subirFoto(@RequestParam("archivo") MultipartFile archivo,@RequestParam("id") Integer id){
//    	@RequestParam("id") Integer id
    	
    	Map<String, Object> response=new HashMap<>();
    	
    	Usuario usuario=usuarioService.getById(id).get();
    	
    	if(!archivo.isEmpty()) {
    		String nombreArchivo= UUID.randomUUID().toString()+"_"+ archivo.getOriginalFilename().replace(" ", "");
    		Path rutaArchivo= Paths.get("ImagenUsuario").resolve(nombreArchivo).toAbsolutePath();
    		
    		try {
    			java.nio.file.Files.copy(archivo.getInputStream(), rutaArchivo);
				
			} catch (Exception e) {
				response.put("mensaje", "Error al subir la imagen");
				return new ResponseEntity<Map<String,Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
			}
    		String nombreFotoAnterior=usuario.getImagen();
    		if(nombreFotoAnterior!=null && nombreFotoAnterior.length()>0) {
    			Path rutaFotoAnterior=Paths.get("ImagenUsuario").resolve(nombreFotoAnterior).toAbsolutePath();
    			File fotoAnterior=rutaFotoAnterior.toFile();
    			if(fotoAnterior.exists() && fotoAnterior.canRead()) {
    				
    				fotoAnterior.delete();
    			}
    			
    		}
    		usuario.setImagen(nombreArchivo);
    		
    		usuarioService.save(usuario);
    		response.put("Usuario", usuario);
    		response.put("mensaje", "Imagen subida con exito: "+ nombreArchivo);
    	}
    	
    	return new ResponseEntity(response, HttpStatus.CREATED);
    }
    
    @GetMapping("/img/{nombreFoto:.+}")
    public ResponseEntity<Resource> verFoto(@PathVariable String nombreFoto){
    	
    	Path rutaArchivo= Paths.get("ImagenUsuario").resolve(nombreFoto).toAbsolutePath();
    	Resource recurso =null;
    	
    	try {
			recurso=new UrlResource(rutaArchivo.toUri());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
    	
    	if(!recurso.exists() && !recurso.isReadable())
    	{
    		throw new RuntimeException("Error no se pudo cargar la imagen:");
    	}
    	org.springframework.http.HttpHeaders cabecera = new org.springframework.http.HttpHeaders();
    	cabecera.add(org.springframework.http.HttpHeaders.CONTENT_DISPOSITION, "Attachment; filename=\""+recurso.getFilename()+"\"");
    	
    	return new ResponseEntity<Resource>(recurso,cabecera,HttpStatus.OK);
    }
	@GetMapping("/UsuarioId/{id}")
	public ResponseEntity<Usuario> getByID(@PathVariable("id")Integer id){
		Usuario user = usuarioService.getById(id).get();
		return  new ResponseEntity<Usuario>(user,HttpStatus.OK);
	}
	
}
