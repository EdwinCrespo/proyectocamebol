package com.CAMEBOl.usuario.security.entity;

public class Mensaje {
	private String texto;

	
	
	public Mensaje(String texto) {
		super();
		this.texto = texto;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}
	
	
}
