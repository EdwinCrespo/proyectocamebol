package com.CAMEBOl.usuario.security.service;

import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;

import com.CAMEBOl.usuario.security.entity.Email;

import ch.qos.logback.core.Context;

@Service
public class EmailService {

	@Value("${mail.urlFront}")
	private String urlFront;
	
	@Autowired
	JavaMailSender javaMailSender;
	
	@Autowired
	TemplateEngine templateEngine;
	public void sendEmail() {
		
		SimpleMailMessage message= new SimpleMailMessage();
		message.setFrom("proyecto.generales.2000@gmail.com");
		message.setTo("e.crespoparada@gmail.com");
		message.setSubject("Pruebas de envio");
		message.setText("Contenido de la prueba");
		
		javaMailSender.send(message);
		
	}
	public void sendEmailTemplate(Email email) {
		MimeMessage message=javaMailSender.createMimeMessage();
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message,true);
			org.thymeleaf.context.Context context= new org.thymeleaf.context.Context();
			Map<String, Object> model = new HashMap<>();
			model.put("userName",email.getUserName());
			model.put("url", urlFront +"/"+ email.getTokenPassword());
			context.setVariables(model);
			String htmlText=templateEngine.process("emailTemplate", context);
			helper.setFrom(email.getMailFrom());
			helper.setTo(email.getMailTo());
			helper.setSubject(email.getSubject());
			helper.setText(htmlText,true);
			javaMailSender.send(message);
			
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
}
