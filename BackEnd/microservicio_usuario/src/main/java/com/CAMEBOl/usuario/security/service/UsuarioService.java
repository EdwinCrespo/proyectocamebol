package com.CAMEBOl.usuario.security.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.CAMEBOl.usuario.security.entity.Usuario;
import com.CAMEBOl.usuario.security.repository.UsuarioRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UsuarioService {
	 @Autowired
	    UsuarioRepository usuarioRepository;

	    public Optional<Usuario> getByNombreUsuario(String nombreUsuario){
	        return usuarioRepository.findByNombreUsuario(nombreUsuario);
	    }
	    public Optional<Usuario> getByNombreUsuarioOrEmail(String nombreUsuarioOrEmail){
	        return usuarioRepository.findByNombreUsuarioOrEmail(nombreUsuarioOrEmail,nombreUsuarioOrEmail);
	    }

	    public Optional<Usuario> getByTokenPassword(String tokenPassword){
	        return usuarioRepository.findByTokenPassword(tokenPassword);
	    }
	    public boolean existsByNombreUsuario(String nombreUsuario){
	        return usuarioRepository.existsByNombreUsuario(nombreUsuario);
	    }

	    public boolean existsByEmail(String email){
	        return usuarioRepository.existsByEmail(email);
	    }
	    public boolean existsById(Integer id){
	        return usuarioRepository.existsById(id);
	    }
	    public void save(Usuario usuario){
	        usuarioRepository.save(usuario);
	    }
	    
	    public List<Usuario> getAll(){
	    	
	    	return usuarioRepository.findAll();
	    }
	    public List<Usuario> getByEstado(String estado){
	    	
	    	return usuarioRepository.getByEstado(estado);
	    }
	    public Optional<Usuario> getById(int id){
	        return usuarioRepository.findById(id);
	    }
	    
	    public void delete(int id) {
	    	usuarioRepository.deleteById(id);
	    }
	    
	    
	    
}
