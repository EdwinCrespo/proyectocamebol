import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { PermissionsGuard } from "../guard/permissions.guard";

import { AdminComponent } from './administrador/container/admin.component';
import { EnterprisingListComponent } from "./administrador/components/enterprising-list/enterprising-list.component";
import { MainComponent } from "./administrador/components/main/main.component";
import { UserListComponent } from "./administrador/components/user-list/user-list.component";

const routes: Routes = [
	{
		path: '', component: AdminComponent, children: //canActivate: [PermissionsGuard], Sirve para proteger a la ruta
			[
				{ path: 'administrador', redirectTo: 'main', pathMatch: 'full' },
				{ path: 'main', component: MainComponent },
				{ path: 'elist', component: EnterprisingListComponent },
				{ path: 'ulist', component: UserListComponent }
			]
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})

export class AdminRoutingModule {

}