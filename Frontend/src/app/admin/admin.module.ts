import { NgModule } from '@angular/core';
import { SharedModule } from '../core/shared/shared.module';
import { AdminRoutingModule } from './admin-routing.module';

import { AdminComponent } from './administrador/container/admin.component';
import { AsideComponent } from './administrador/components/aside/aside.component';
import { MainComponent } from './administrador/components/main/main.component';
import { RightSectionComponent } from './administrador/components/right-section/right-section.component';
import { UserListComponent } from './administrador/components/user-list/user-list.component';
import { EnterprisingListComponent } from './administrador/components/enterprising-list/enterprising-list.component';

import { NewUserComponent } from './administrador/components/new-user/new-user.component';
import { EditUserComponent } from './administrador/components/edit-user/edit-user.component';
import { DeleteUserComponent } from './administrador/components/delete-user/delete-user.component';

import { FilterPipe } from './administrador/pipes/filter.pipe';

@NgModule({
    imports: [
        AdminRoutingModule,
        SharedModule
    ],
    declarations: [
        AdminComponent,
        AsideComponent,
        MainComponent,
        RightSectionComponent,
        UserListComponent,
        EnterprisingListComponent,
        NewUserComponent,
        EditUserComponent,
        DeleteUserComponent,
        FilterPipe
    ],
    exports: [],
    providers: [
    ]
})

export class AdminModule {
  constructor() { }
}