import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Usuario } from 'src/app/core/models/Usuario';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.css']
})
export class DeleteUserComponent implements OnInit {

  id:number=0;
  nombre: string = "";
  primerApellido: string = "";
  segundoApellido: string = "";
  fechaNacimiento: Date = new Date('2018-12-03');
  ci: string = "";
  email: string = "";
  genero: string = "";
  opcion: string = "";
  numeroTelefono:string="";
  event: Date=new Date('2018-12-03');
  usuario?:Usuario;
  constructor(@Inject(MAT_DIALOG_DATA) private data: {id: number, nombre: string, primerApellido: string,segundoApellido:string,fechaNacimiento:Date,numeroTelefono:string,ci:string,email:string},public Dialog:MatDialog,private authService: AuthService) { 
    this.id=data.id;
    this.event=new Date(data.fechaNacimiento);
    this.nombre = data.nombre;
    this.primerApellido = data.primerApellido;
    this.segundoApellido=data.segundoApellido;
    this.fechaNacimiento=this.event;
    this.numeroTelefono=data.numeroTelefono;
    this.ci=data.ci;
    this.email=data.email;
  }

  ngOnInit(): void {
  }

  eliminar(): void {
    this.usuario= new Usuario(this.id,this.nombre,this.email,this.primerApellido,this.segundoApellido,this.fechaNacimiento,this.numeroTelefono,this.ci,"0");
    this.authService.actualizar(this.usuario).subscribe({
      next: (v) => alert("Usuario eliminado"),
      error: (e) => alert(e.error.texto),
      complete: () => console.info('complete') 
  });
  this.Dialog.closeAll();
  window.location.reload();
    // alert('Eliminado'); // llamar al metodo eliminar
    
  }

  cancelar(): void {
    alert('cancelado');
    this.Dialog.closeAll();
  }
}
