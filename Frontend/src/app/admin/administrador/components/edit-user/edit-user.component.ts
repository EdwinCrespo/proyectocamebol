import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Route, Router } from '@angular/router';
import { Usuario } from 'src/app/core/models/Usuario';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  id:number=0;
  imagen:string="";
  nombre: string = "";
  primerApellido: string = "";
  segundoApellido: string = "";
  fechaNacimiento: Date = new Date('2018-12-03');
  ci: string = "";
  email: string = "";
  genero: string = "";
  opcion: string = "";
  numeroTelefono:string="";
  event: Date=new Date('2018-12-03');
  usuario?:Usuario;
  usuario2?:Usuario;
  imagenNueva?:File;
  // fileName:File;
  constructor(@Inject(MAT_DIALOG_DATA) private data: {id: number, nombre: string, primerApellido: string,segundoApellido:string,fechaNacimiento:Date,numeroTelefono:string,ci:string,email:string,imagen:string},private authService: AuthService,private dialog: MatDialog,private route:Router) { 
    this.id=data.id;
    this.event=new Date(data.fechaNacimiento);
    this.nombre = data.nombre;
    this.primerApellido = data.primerApellido;
    this.segundoApellido=data.segundoApellido;
    this.fechaNacimiento=this.event;
    this.numeroTelefono=data.numeroTelefono;
    this.ci=data.ci;
    this.email=data.email;
    this.imagen=data.imagen;
  }

  ngOnInit(): void {
    if(this.imagen==null){
      this.imagen="sm.png";
    }
  
  }

  actualizar(): void {
    this.onUpdate();
  }
  onUpdate():void {
    this.usuario= new Usuario(this.id,this.nombre,this.email,this.primerApellido,this.segundoApellido,this.fechaNacimiento,this.numeroTelefono,this.ci,"1");
    this.authService.actualizar(this.usuario).subscribe({
      next: (v) => alert(v.texto),
      error: (e) => alert(e.error.texto),
      complete: () => console.info('complete') 
  });
  this.dialog.closeAll();
  window.location.reload();
}
  generoChanged(): void {
    this.genero = this.opcion;
  }

  subirFoto(){
    // alert("hola");
    // this.authService.subirImagen(this.imagenNueva!);

    console.log(this.imagenNueva);

    if (this.imagenNueva) {
      this.authService.subirImagen(this.imagenNueva,this.id).subscribe(resp => {
        alert("Foto actualizada")
        window.location.reload();
      })
    } else {
      alert("Seleccionar una imagen")
    }
  }
  capturar(fileInput:Event){

      this.imagenNueva= (<HTMLInputElement>fileInput.target).files[0];

  }
  
}


