import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NuevoUsuario } from 'src/app/core/models/NuevoUsuario';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {

  nombre: string = "";
  primerApellido: string = "";
  segundoApellido: string = "";
  fechaNacimiento: Date = new Date('2018-12-03');
  ci: string = "";
  email: string = "";
  genero: string = "";
  opcion: string = "";
  telefono:string=""; 

  aleatorio:number=0;
  nombreUsuario:string="";
  password:string="";
  nuevoUsuario?:NuevoUsuario
  constructor(public dialog: MatDialog, private route: Router,  private authService: AuthService) { }

  ngOnInit(): void {
  }

  registrar(): void{
    this.aleatorio=Math.random()*1000;
    this.aleatorio=Math.floor(this.aleatorio);// concatenar esta variable al password
    this.password="HolaMundo";
    this.nombreUsuario=this.nombre+this.ci;
    this.nuevoUsuario= new NuevoUsuario(this.nombre,this.nombreUsuario,this.email,this.password,this.primerApellido,this.segundoApellido,this.ci,this.telefono,this.genero,this.fechaNacimiento,1,"1");
    this.authService.nuevo(this.nuevoUsuario).subscribe({
      next: (v) => alert(v.texto),
      error: (e) => alert(e.error.texto),
      complete: () => console.info('complete') 
  });
  this.dialog.closeAll();
  window.location.reload();
  // alert(this.aleatorio);
  }

  generoChanged(): void {
    this.genero = this.opcion;
  }
}
