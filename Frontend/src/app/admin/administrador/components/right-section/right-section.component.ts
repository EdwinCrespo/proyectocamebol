import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/core/models/Usuario';
import { AuthService } from 'src/app/services/auth.service';
import { TokenService } from 'src/app/services/token.service';
@Component({
  selector: 'app-right-section',
  templateUrl: './right-section.component.html',
  styleUrls: ['./right-section.component.css']
})
export class RightSectionComponent implements OnInit {

  constructor(private authService: AuthService, public tokenService: TokenService) { }

  usuario?:Usuario;

  ngOnInit(): void {

    this.capturarUsuario();

  }

  public capturarUsuario():void{
    this.authService.ListaUsuarioId(Number.parseInt(sessionStorage.getItem("IdUsuario")!?.toString())).subscribe({
      next:(data:Usuario)=>{
        this.usuario=data;
      },
      error:(e)=>{alert(e.error.texto)}
  });
   
  }

}
