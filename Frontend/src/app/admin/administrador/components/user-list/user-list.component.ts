import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Usuario } from 'src/app/core/models/Usuario';
import { AuthService } from 'src/app/services/auth.service';
import { NewUserComponent } from '../new-user/new-user.component';
import { EditUserComponent } from '../edit-user/edit-user.component';
import { DeleteUserComponent } from '../delete-user/delete-user.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})

export class UserListComponent implements OnInit {

  search: string = '';
  usuarios: Usuario[] = [];
  page: number = 0;
  show_page: number = 0;

  constructor(public dialog: MatDialog, private router: Router, private route: ActivatedRoute, private authService: AuthService) {

  }

  ngOnInit(): void {
    this.cargarUsuarios();
  }

  crearUsuario(): void {
    this.dialog.closeAll();

    const modalRef = this.dialog.open(NewUserComponent, {
      width: '350px',
    });
  }

  editarUsuario(id: number, nombre: string, primerApellido: string,segundoApellido:string,fechaNacimiento:Date,numeroTelefono:string,ci:string,email:string,imagen:string): void {
    this.dialog.closeAll();

    const modalRef = this.dialog.open(EditUserComponent, {
      width: '350px',
      data: { id: id,
              nombre: nombre,
              primerApellido: primerApellido,
              segundoApellido:segundoApellido,
              fechaNacimiento: fechaNacimiento,
              numeroTelefono:numeroTelefono,
              ci:ci,
              email:email,
              imagen:imagen},
    });
  }

  eliminarUsuario(id: number, nombre: string, primerApellido: string,segundoApellido:string,fechaNacimiento:Date,numeroTelefono:string,ci:string,email:string): void {
    this.dialog.closeAll();

    const modalRef = this.dialog.open(DeleteUserComponent, {
      width: '250px',
      data: { id: id,
        nombre: nombre,
        primerApellido: primerApellido,
        segundoApellido:segundoApellido,
        fechaNacimiento: fechaNacimiento,
        numeroTelefono:numeroTelefono,
        ci:ci,
        email:email},
    });
  }

  cargarUsuarios(): void {
    this.authService.listaEstado("1").subscribe(
      data => {
        this.usuarios = data;
      }, err => {
        console.log(err);
      }
    );
  }

  nextPage() {
    let show = this.show_page === 0 ? 5 : this.show_page;
    if(this.page === -1) {
      this.page = this.usuarios.length / show * show + 5;
    }
    else 
      this.page += show;
  }

  prevPage() {
    this.page -= this.show_page === 0 ? 5 :  this.show_page;
  }

  firstPage() {
    this.page = 0;
  }

  lastPage() {
    this.page = -1;
  }

  searchUser(search: string) {
    this.search = search;
  }

  showChanged(show: string) {
    this.show_page = parseInt(show);
  }
}