import { Pipe, PipeTransform } from '@angular/core';
import { Usuario } from 'src/app/core/models/Usuario';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(usuario: Usuario[], show_page: number = 0, page: number = 0, search: string = ''): Usuario[] {

    const filterUsers = usuario.filter(user => {
      if (user.nombre.includes(search))
        return true;
      else if (user.primerApellido.includes(search))
        return true;
      else if (user.segundoApellido != null) {
        user.segundoApellido.includes(search);
      }
      else if (user.numeroTelefono.includes(search))
        return true;
      else if (user.nombreUsuario.includes(search))
        return true;
      else if (user.email.includes(search))
        return true;

      return false;
    });

    if(page === -1) {
      let show = show_page === 0 ? 5 : show_page;
      let newPage = usuario.length / show * show;

      console.log(usuario.length - show + " " + (newPage + show));
      if (usuario.length % show === 0)
        return usuario.slice(usuario.length - show, newPage);  
      else 
        return usuario.slice(newPage, newPage + show );
    }

    if (search.length === 0) {
      if (show_page === 0 || show_page === 5)
        return usuario.slice(page, page + 5)
      else 
        return usuario.slice(page, page + 10);
    } else {
      console.log(filterUsers);
      if (show_page === 0 || show_page === 5)
        return filterUsers.slice(page, page + 5)
      else if (show_page === 10)
        return filterUsers.slice(page, page + 10);
      else
        return filterUsers.slice(page, page + 15);
    }
  }
}
