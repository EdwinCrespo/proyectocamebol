export class NuevoUsuario {
  idUsuario: number = 0;
  nombre: string = '';
  primerApellido: string = '';
  segundoApellido: string = '';
  fechaNacimiento: Date = new Date();
  imagen: number = 0;
  email: string = '';
  numeroTelefono: string = '';
  nombreUsuario: string = '';
  password: string = '';
  authorities:string[]=[];
  idEmpresa: number = 0;
  fechaRegistro: Date = new Date();
  fechaActualizacion: Date = new Date();
  estado: string = "";
  ci: string = "";
  genero: string = "";
  constructor(nombre:string,nombreUsuario:string,email:string,password:string,primerApellido:string,segundoApellido:string,ci:string,numeroTelefono:string,genero:string,fechaNacimiento:Date,idEmpresa:number,estado:string) {
    this.nombre=nombre;
    this.nombreUsuario=nombreUsuario;
    this.email=email;
    this.password=password;
    this.primerApellido=primerApellido;
    this.segundoApellido=segundoApellido;
    this.ci=ci;
    this.numeroTelefono=numeroTelefono;
    this.genero=genero;
    this.fechaNacimiento=fechaNacimiento;
    this.idEmpresa=idEmpresa;
    this.estado=estado;
  }
}
