export class Usuario {
    id: number = 0;
    nombre: string = '';
    primerApellido: string = '';
    segundoApellido: string = '';
    fechaNacimiento: Date = new Date();
    imagen: string = "";
    email: string = '';
    numeroTelefono: string = '';
    nombreUsuario: string = '';
    password: string = '';
    authorities:string[]=[];
    idEmpresa: number = 0;
    fechaRegistro: Date = new Date();
    fechaActualizacion: Date = new Date();
    estado: string = "";
    ci: string = "";
    constructor(id:number,nombre:string,email:string,primerApellido:string,segundoApellido:string,fechaNacimiento:Date,numeroTelefono:string,ci:string,estado:string) {
      this.id=id;
      this.nombre=nombre;
      this.email=email;
      this.primerApellido=primerApellido;
      this.segundoApellido=segundoApellido;
      this.fechaNacimiento=fechaNacimiento;
      this.numeroTelefono=numeroTelefono;
      this.ci=ci;
      this.estado=estado;
    }
  }