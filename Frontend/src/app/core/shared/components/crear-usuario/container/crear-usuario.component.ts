import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NuevoUsuario } from 'src/app/core/models/NuevoUsuario';
import { AuthService } from 'src/app/services/auth.service';
import { LoginComponent } from 'src/app/public/usuarios/login/container/login.component';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css']
})
export class CrearUsuarioComponent implements OnInit {

  constructor(public dialog: MatDialog, private route: Router,  private authService: AuthService) { }
  nombre: string = "";
  primerApellido: string = "";
  segundoApellido: string = "";
  fechaNacimiento: Date = new Date('2018-12-03');
  ci: string = "";
  email: string = "";
  password: string = "";
  password2: string = "";
  genero:string="";
  telefono:string="";
  
  nombreUsuario:string="";


  isRegister=false;
  isRegisterFaild=false;
  nuevoUsuario?:NuevoUsuario
  ngOnInit(): void {
  }
  onRegister():void {
    this.nuevoUsuario= new NuevoUsuario(this.nombre,this.nombreUsuario,this.email,this.password,this.primerApellido,this.segundoApellido,this.ci,this.telefono,this.genero,this.fechaNacimiento,1,"1");
    this.authService.nuevo(this.nuevoUsuario).subscribe({
      next: (v) => alert(v.texto),
      error: (e) => alert(e.error.texto),
      complete: () => console.info('complete') 
  });
    
    
    // .subscribe(data=>{
    //     this.isRegister=true;
    //     this.isRegisterFaild=false;
    //     alert("Registro Exitoso");
    //     this.route.navigate(['/home']);
    //   },
    //   err=>{
    //     this.isRegister=false;
    //     this.isRegisterFaild=true;
    //     // this.errMsj=err.error.Mensaje;
    //     alert(err.error);
    //   }
    //   );
    
    
    

  
    
   
  }
  openLogin(): void{
    this.dialog.closeAll();

    const modalRef = this.dialog.open(LoginComponent, {
      width: '280px',
    });

    modalRef.afterClosed().subscribe(verificar => {
      console.log('Modal login');
      console.log(verificar);
    });

  }
  registrar(): void {
  // alert(this.fechaNacimiento);
    this.onRegister();
    // alert(this.genero);
  }
}
