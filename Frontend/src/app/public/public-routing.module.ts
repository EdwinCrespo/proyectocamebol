import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { PublicComponent } from './public.component';
import { HomeComponent } from "./home/container/home.component";
import { InicioComponent } from "./inicio/container/inicio.component";
import { QuienesComponent } from './quienes/container/quienes.component';
import { ContactoComponent } from "./contacto/container/contacto.component";
import { LoginComponent } from "./usuarios/login/container/login.component";
import { CambiarContraseniaComponent } from "./usuarios/cambiar-contrasenia/cambiar-contrasenia.component";

const routes: Routes = [
	{
		path: '', component: PublicComponent, children:
			[
				{ path: '', redirectTo: 'inicio', pathMatch: 'full' },
				{ path: 'home', component: HomeComponent },
				{ path: 'inicio', component: InicioComponent },
				{ path: 'quienes', component: QuienesComponent }, //Para pasar parametros dinamicos ponemos { path: 'inicio/:id'}
				{ path: 'contacto', component: ContactoComponent },
				{ path: 'cambiar-contrasenia/:tokenPassword', component: CambiarContraseniaComponent },
			]
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})

export class PublicRoutingModule {

}