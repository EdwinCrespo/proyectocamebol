import { NgModule } from '@angular/core';
import { SharedModule } from '../core/shared/shared.module';
import { PublicRoutingModule } from './public-routing.module';

import { PublicComponent } from './public.component';
import { HomeComponent } from './home/container/home.component';
import { InicioComponent } from './inicio/container/inicio.component';
import { QuienesComponent } from './quienes/container/quienes.component';
import { ContactoComponent } from './contacto/container/contacto.component';
import { LoginComponent } from './usuarios/login/container/login.component';
import { EmailRecuperacionComponent } from './usuarios/email-recuperacion/email-recuperacion.component';
import { CambiarContraseniaComponent } from './usuarios/cambiar-contrasenia/cambiar-contrasenia.component';

@NgModule ({
    imports: [
        PublicRoutingModule,
        SharedModule
    ],
    declarations: [
        PublicComponent,
        HomeComponent,
        LoginComponent,
        InicioComponent,
        QuienesComponent,
        ContactoComponent,
        EmailRecuperacionComponent,
        CambiarContraseniaComponent
    ],
    exports: [],
    providers: []
})

export class PublicModule {
    constructor () {}
}