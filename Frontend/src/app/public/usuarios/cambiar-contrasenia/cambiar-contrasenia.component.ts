import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { CambiarPassword } from 'src/app/core/models/CambiarPassword';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-cambiar-contrasenia',
  templateUrl: './cambiar-contrasenia.component.html',
  styleUrls: ['./cambiar-contrasenia.component.css']
})
export class CambiarContraseniaComponent implements OnInit {

  constructor(private activateRoute:ActivatedRoute,private dialog: MatDialog,
    public authService: AuthService,
    private router: Router) { }
  password:string="";
  password2:string="";
  tokenPassword:string="";
  cambiarPassword?:CambiarPassword;
 // alert(this.activateRoute.snapshot.params['tokenPassword']);
  ngOnInit(): void {
    
  }
  Actualizar(){
    this.tokenPassword=this.activateRoute.snapshot.params['tokenPassword'];
    this.cambiarPassword=new CambiarPassword(this.password,this.password2,this.tokenPassword);
    this.authService.CambiarPassword(this.cambiarPassword).subscribe({
      next: (v) => {
        alert(v.texto)
        this.router.navigate(['/']);
      },
      error: (e) => alert(e.error.texto),
      complete: () => console.info('complete') 
  });
  }
}
