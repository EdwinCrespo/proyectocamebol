import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { UsuarioService } from '../../../../services/usuario.service';
import { NuevoUsuario } from 'src/app/core/models/NuevoUsuario';
import { Router } from '@angular/router';
import { TokenService } from 'src/app/services/token.service';
import { AuthService } from 'src/app/services/auth.service';
import { LoginUsuario } from 'src/app/core/models/login-usuario';
import { RecuperarContraseniaComponent } from '../../recuperar-contrasenia/recuperar-contrasenia.component';
import { CrearUsuarioComponent } from 'src/app/core/shared/components/crear-usuario/container/crear-usuario.component';
import { EmailRecuperacionComponent } from '../../email-recuperacion/email-recuperacion.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  //verificar = 'algo'
  // usuario?: NuevoUsuario;

  error: string = "none";
  // contructor para inyectar servicios

  isLogged=false;
  isLoginFail=false;
  loginUsuario?: LoginUsuario;
  pass: string = "";
  userName: string = "";
  roles: string[]=[];
  errMsj: string="";
  rolesw: string[]=[];
  constructor(
    private dialog: MatDialog,
    public tokenService: TokenService,
    public authService: AuthService,
    private router: Router
    ) { }
  // ngOnInit para inicializar los datos del backend que queremos mostrar
  ngOnInit(): void {
    if(this.tokenService.getToken()){
      this.isLogged=true;
      this.isLoginFail=false;
      this.roles=this.tokenService.getAuthorities();
    }
  }
 onLogin():void {
  this.loginUsuario= new LoginUsuario(this.userName,this.pass);
  this.authService.login(this.loginUsuario).subscribe(data=>{
    this.isLogged=true;
    this.isLoginFail=false;

    this.tokenService.setToken(data.token);
    this.tokenService.setUserName(data.nombreUsuario);
    this.tokenService.setAuthorities(data.authorities);
    this.tokenService.setId(data.idUsuario)
    this.roles=data.authorities;

    this.rolesw=this.tokenService.getAuthorities();
    alert("Bienvenido  "+this.tokenService.getUserName()+"  ID:  "+this.tokenService.getId()+"   roles"+ this.rolesw.length);
    if(this.rolesw.length>1){
      this.router.navigate(['/administrador']);
      this.dialog.closeAll();
    }
    else{
      this.router.navigate(['/home']);
      this.dialog.closeAll();
    }
  },
  err=>{
    this.isLogged=false;
    this.isLoginFail=true;
    // this.errMsj=err.error.Mensaje;
    alert("Error en el login");
  }
  );
}


  openRegistrarUsuario(): void {
    this.dialog.closeAll();
    const modalRef = this.dialog.open(CrearUsuarioComponent, {
      width: '550px',
    });

    modalRef.afterClosed().subscribe(verificar => {
    });
  }

  openRecuperarContrasenia() {
    this.dialog.closeAll();
    const modalRePass = this.dialog.open(EmailRecuperacionComponent, {
      width: '350px',
    });
  }

  autenticarUsuario(): void {
    this.onLogin();
  }
}
