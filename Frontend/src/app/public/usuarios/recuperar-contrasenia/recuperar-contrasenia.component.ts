import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Email } from 'src/app/core/models/Email';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-recuperar-contrasenia',
  templateUrl: './recuperar-contrasenia.component.html',
  styleUrls: ['./recuperar-contrasenia.component.css']
})
export class RecuperarContraseniaComponent implements OnInit {


  correo:string="";
  email?:Email;
  constructor(  private dialog: MatDialog,
    public authService: AuthService,
    private router: Router) { }

  ngOnInit(): void {
  }
  Enviar():void{
    this.email= new Email(this.correo);
    this.authService.EnviarEmailPassword(this.email).subscribe({
      next: (v) => alert(v.texto),
      error: (e) => alert(e.error.texto),
      complete: () => console.info('complete') 
  });
  }
}
