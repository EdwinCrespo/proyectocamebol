import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CambiarPassword } from '../core/models/CambiarPassword';
import { Email } from '../core/models/Email';
import { JwtDto } from '../core/models/jwt-dto';
import { LoginUsuario } from '../core/models/login-usuario';
import { NuevoUsuario } from '../core/models/NuevoUsuario';
import { Usuario } from '../core/models/Usuario';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authURL='http://localhost:8080/auth/'
  constructor(private httpClient: HttpClient) {}
   //aqui va el metodo del CRUD usuario

    public nuevo(nuevoUsuario:NuevoUsuario):Observable<any>{
      return this.httpClient.post<any>(this.authURL+"nuevo",nuevoUsuario)
    }

   public login(loginUsuario:LoginUsuario):Observable<JwtDto>{
    return this.httpClient.post<JwtDto>(this.authURL+"login",loginUsuario)
   }
   public lista():Observable<Usuario[]>{
    return this.httpClient.get<Usuario[]>(this.authURL+"user/lista")
  }
  public listaEstado(estado:string):Observable<Usuario[]>{
    return this.httpClient.get<Usuario[]>(this.authURL+"user/listaEstado/"+estado)
  }
  public actualizar(usuario:Usuario):Observable<any>{
    return this.httpClient.put<any>(this.authURL+"user/actualizar/"+usuario.id,usuario)
  }
  public ListaUsuarioId(id:number):Observable<Usuario>{
    return this.httpClient.get<Usuario>(this.authURL+"user/UsuarioId/"+id)
  }
  public subirImagen(archivo:File,id:number):Observable<any>{
    let formData=new FormData();
    formData.append("archivo",archivo);
   formData.append("id",id.toString());
    return this.httpClient.post<any>(this.authURL+"user/subirFoto",formData)
  }
  public EnviarEmailPassword(email:Email):Observable<any>{
    return this.httpClient.post<any>(this.authURL+"email/enviar",email)
   }
   public CambiarPassword(cambiarPassword:CambiarPassword):Observable<any>{
    return this.httpClient.post<any>(this.authURL+"email/cambiarPassword",cambiarPassword)
   }
}
