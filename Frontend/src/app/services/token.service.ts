import { Injectable } from '@angular/core';

const TOKEN_KEY='AuthToken';
const USERNAME_KEY='AuthUserName';
const AUTHORITIES_KEY='AuthAuthorities';
const ID_KEY="IdUsuario";
@Injectable({
  providedIn: 'root'
})
export class TokenService {
  roles: Array<string>=[];
  constructor() { }
  public setToken(token: string):void{
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY,token);
  }
  public getToken():string{
   
   return sessionStorage.getItem(TOKEN_KEY)??"";
  }
  public setUserName(userName: string):void{
    window.sessionStorage.removeItem(USERNAME_KEY);
    window.sessionStorage.setItem(USERNAME_KEY,userName);
  }
  public getUserName():string{
   
   return sessionStorage.getItem(USERNAME_KEY)??"";
  }
  public setId(idUsuario: number):void{
    window.sessionStorage.removeItem(ID_KEY.toString());
    window.sessionStorage.setItem(ID_KEY.toString(),idUsuario.toString());
  }
  public getId():string{
   
   return sessionStorage.getItem(ID_KEY.toString())??"";
  }
  public setAuthorities(authorities: string[]):void{
    window.sessionStorage.removeItem(AUTHORITIES_KEY);
    window.sessionStorage.setItem(AUTHORITIES_KEY,JSON.stringify(authorities));
  }
  public getAuthorities():string[]{
   
    this.roles=[];
    if(sessionStorage.getItem(AUTHORITIES_KEY)){
      
      JSON.parse(sessionStorage.getItem(AUTHORITIES_KEY)||"").forEach((authority: { authority: string; }) =>{
        this.roles.push(authority.authority);
      });
    }
    return this.roles;
   }

   public logOut(): void{
    window.sessionStorage.clear();
   }
}
