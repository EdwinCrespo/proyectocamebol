import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NuevoUsuario } from '../core/models/NuevoUsuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private baseEndpoint = 'http://localhost:8090/api/usuario/login';
  private cabeceras: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(private http: HttpClient) { }

  //Mandamos el login y password y nos retorna el usuario si existe, se puede retornar un booleano, pero por el momento esta asi.
  public login(nombreUsuario: string, contrasenia: string): Observable<NuevoUsuario> {
    return this.http.get<NuevoUsuario>(`${this.baseEndpoint}/${nombreUsuario}/${contrasenia}`);
  }

  public crear(usuario: NuevoUsuario): Observable<NuevoUsuario> {
    return this.http.post<NuevoUsuario>(this.baseEndpoint, usuario, { headers: this.cabeceras });
  }

}
